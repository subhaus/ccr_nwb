import React, { Component } from 'react';

import qs from 'qs';

import Product from './Product';
import Spinner from './Spinner';
import ReactPaginate from 'react-paginate';
import Select from 'react-select';
import SideFilterGroup from './form_items/SideFilterGroup';

import FilterByVehicleTeilen from '../forms/FilterByVehicleTeilen';

import { basePath, getURLQuery,
   baseApiUrl, checkStatus, returnJSON, URLParamsToObject,
   scrollToTop, getPathname } from '../lib/constants';
import { sortOptions, perPageOptions } from '../lib/helpers';

import anime from 'animejs';

let cat_id = getURLQuery('typs');
let type = getURLQuery('type');
let adresa = 'filter_teilen';

class ResultsTeilen extends Component {

   state = {
      data: [],
      limit: 12,
      totalPages: 1,
      page: 0,
      currPage: 0,
      title: '',
      sort: 'pd.name',
      order: 'ASC',
      error: false,
      loading: true,
      query: {
         typs: cat_id,
         type: type,
         sort: 'pd.name',
         order: 'ASC',
         limit: 12,
         page: 0
      },
      sideQuery: {},
      search: [],
      activeBrands: [],
      selectedBrands: [],
      message: ''
   };

   constructor(props) {
      super(props);
      this.handleCheckbox = this.handleCheckbox.bind(this);
   }


   componentDidMount() {
      if(getURLQuery('brands')) {
         let selectedBrands = getURLQuery('brands');
         selectedBrands = selectedBrands.split('-');
         this.setState({ selectedBrands });
      }
      this.getActiveBrands(decodeURIComponent(this.props.location.search.replace('?','&')));
      this.getData(decodeURIComponent(this.props.location.search.replace('?','&')));

   }

   componentWillReceiveProps(newProps) {
      if(newProps.location.search !== this.props.location.search) {
         this.fade(0);
         if(getURLQuery('brands')) {
            let selectedBrands = getURLQuery('brands');
            selectedBrands = selectedBrands.split('-');
            this.setState({ selectedBrands });
         }
         this.getActiveBrands(decodeURIComponent(newProps.location.search.replace('?','&')));
         this.getData(decodeURIComponent(newProps.location.search.replace('?','&')));
      }
   }


   getData(query) {

      adresa = this.state.sideQuery.hasOwnProperty('marka') ? 'filter_teilen/get_product_data' : 'filter_teilen';
      this.setState({ loading: true });

      fetch(baseApiUrl+adresa+query, {
         method: "GET",
         credentials: "same-origin"
      }).then(checkStatus).then(returnJSON)
      .then(data => {
         if(data.count >= 0) {
            this.fade(1);
            this.setState({ data: data.result, totalPages: data.totalPages, error: false }, () => {
               this.setState({ loading: false });
            });
         } else {
            this.setState({ error: true, message: 'There is no products...', loading: false });
         }
      }).catch(err => {
         this.setState({ error: true, message: 'Error! Please, refresh the page and try again.' });
         console.log(err);
      });
   }


   getActiveBrands(query) {
      fetch(baseApiUrl+'filter_teilen/get_attributes&wanted=brands'+query, {
         method: "GET",
         credentials: "same-origin"
      }).then(checkStatus).then(returnJSON)
      .then(activeBrands => {
         this.setState({ activeBrands });
      }).catch(err => {
         this.setState({ error: true, message: 'Error! Please, refresh the page and try again.' });
         console.log(err);
      });
   }

   getParams() {
      scrollToTop();
      let query = this.state.query;
      if(query.brands !== null || query.brands !== '' || typeof query.brands !== undefined) {
         query.brands = this.state.selectedBrands.join('-');
      } else {
         delete query.brands;
      }
      query = Object.assign(query, this.state.sideQuery);
      let qstring = qs.stringify(query, { encode: false, addQueryPrefix: true, skipNull: true });
      this.props.history.replace(basePath+'filter_teilen' + qstring);
   }

   handlePageChange = (page) => {
      let query = this.state.query;
      query.page = page.selected;
      this.setState({ query }, this.getParams);
   };

   handleLimitChange = (num) => {
      let query = this.state.query;
      query['limit'] = num.value;
      query.page = 0;
      this.setState({ query }, this.getParams);
   };

   handleSortChange = (val) => {
      let sortOrder = val.value.split("|");
      let query = this.state.query;
      query['sort'] = sortOrder[0];
      query['order'] = sortOrder[1];
      query.page = 0;
      this.setState({ query }, this.getParams);
   };

   filterResults() {
      return this.state.data.map((val, key) => {
         return <Product key={key} data={val} />
      });
   }

   updateQuery(sideQuery) {
      let query = this.state.query;
      query.page = 0;
      this.setState({ sideQuery, query }, this.getParams);
   }

   submitQuery(sideQuery) {
      let query = this.state.query;
      query.page = 0;
      this.setState({ sideQuery, query }, this.getParams);
   }


   fade(opacity) {
      anime({
         targets: '#filter_result .products',
         opacity: opacity,
         easing: 'easeInOutQuad',
         duration: 250,
      });
   }

   handleCheckbox = (selectedBrands) => {
      let query = this.state.query;
      query.page = 0;
      this.setState({ selectedBrands, query }, this.getParams);
   }

   render() {

      let spinnerStyle = {
           position: 'absolute',
           top: 'calc(40vh - 37px)',
           left: 'calc(50% - 37px)'
      }

      let results = this.filterResults();

      let pagination = (this.state.totalPages <= 1) ? '' :
         <div className="pagination_wrapper">
            <ReactPaginate
               pageCount={this.state.totalPages}
               marginPagesDisplayed={2}
               pageRangeDisplayed={5}
               containerClassName={"pagination"}
               subContainerClassName={"pages pagination"}
               activeClassName={"active"}
               forcePage={parseInt(this.state.query.page)}
               onPageChange={this.handlePageChange} />
         </div>;

      let spinner = '';
      if(this.state.loading) {
         spinner = <Spinner style={spinnerStyle} />;
      }

      let error = (this.state.error === true && !this.state.loading) ? <p className="text-center">{this.state.message}</p> : '';

      return (

         <div className="row">

            <aside id="side_filter" className="col-lg-2">

               <div className="clearfix">
                  <SideFilterGroup
                     selected={this.state.selectedBrands}
                     className="side_filter_group marken clearfix"
                     ref="marken"
                     source="filter_teilen/get_category_brands&typs=51-52-53"
                     activeItems={this.state.activeBrands}
                     id="side_filter_marken"
                     valName="brands"
                     onChange={this.handleCheckbox}
                     title="Marken" />
               </div>

               <FilterByVehicleTeilen type={type} onChange={this.submitQuery.bind(this)} />

            </aside>

            <div id="products_cont" className="products_cont col-lg-10">
               <div className="row">
                  <h1 className="page_title">Filter results</h1>
               </div>
               <div className="row">

                  <div id="filter_result" className="filter_result">

                     <div className="row filterOptions">
                        <div className="col-lg-2 perPage">
                           <label>Anzeige:</label>
                           <Select clearable={false}
                              className="perPage"
                              value={parseInt(this.state.query.limit)}
                              options={perPageOptions}
                              onChange={this.handleLimitChange.bind(this)}
                              name="limit" />
                        </div>

                        <div className="col-lg-4 sortBy">
                           <label>Sortieren nach:</label>
                           <Select clearable={false}
                              value={this.state.query.sort+'|'+this.state.query.order}
                              options={sortOptions}
                              onChange={this.handleSortChange.bind(this)}
                              name="selekt" />
                        </div>
                     </div>

                     <div className="products clearfix">
                        {results}
                        {pagination}
                     </div>
                     <div className="clearfix"></div>

                     {error}
                  </div>
                  {spinner}
               </div>
            </div>
         </div>
      );
   }
}


// ResultsTeilen.propTypes = {
// 	source: PropTypes.array,
// 	max: PropTypes.number
// };

ResultsTeilen.defaultProps = {
	source: [],
	max: 1
};

export default ResultsTeilen;
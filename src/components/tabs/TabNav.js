import React, { Component } from 'react';
import PropTypes from 'prop-types';

const TabNav = ({ children }) => {
   return <nav className="tab_nav">{children}</nav>
}

export default TabNav;
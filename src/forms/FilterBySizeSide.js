import React, { Component } from 'react';
import { withRouter } from 'react-router';

import qs from 'qs';

import Select from 'react-select';
import { Tire } from '../svg/Icons';
import { basePath, baseUrl, baseApiUrl, checkStatus, returnJSON, serialize } from '../lib/constants';

class FilterBySizeSide extends Component {

   state = {
      sizes: {},
      breite: null,
      hoehe: null,
      zoll: null,
      tireColors: ['#666', '#666', '#666']
   };

   componentDidMount() {
      let atts = {};
      if(this.props.query.hasOwnProperty('atts')) {
         atts = this.props.query.atts;
         for(let key in atts) {
            this.setState({ [key]: atts[key] });
         }
      }
      this.setState({ sizes: atts });
   }

   handleFocus = (e) => {
      var colors = [];
      for(var i = 0; i < 3; i++) {
         colors[i] = '#666';
         if(i === e) {
            colors[i] = '#ff4500';
         }
      }
      this.setState({ tireColors: colors });
   };

   handleSelectChange = (key, val) => {
      let sizes = this.state.sizes;
      let params = this.props.query;

      if(val === null) {
         this.setState({ [key]: null });
         delete sizes[key];
      } else {
         this.setState({ [key]: val.value });
         sizes[key] = val.value;
      }
      params.atts = sizes;
      if(params.hasOwnProperty('page')) { delete params.page }
      this.props.history.push(basePath+'filter'+qs.stringify(params, { encode: false, addQueryPrefix: true, skipNull: true }));
   }

   render() {

      return (
         <div className="side_filter_group reifengrosse clearfix">
            <h3 className="subtitle">
               <a role="button" href={'#side_filter_reifengrosse'} data-toggle="collapse" data-parent="side_filter" className="collapsed">Filter By Size</a>
            </h3>

            <form id="side_filter_reifengrosse" className="collapse">

               <Tire id="tire" colors={this.state.tireColors} style={{width:'100%', height:'100%'}} />

               <Select clearable={true}
                  placeholder="Breite"
                  name="atts[breite]"
                  value={this.state.breite}
                  options={this.props.options.breite}
                  onFocus={this.handleFocus.bind(this, 0)}
                  onChange={this.handleSelectChange.bind(this, 'breite')} />

               <Select clearable={true}
                  placeholder="Hoehe"
                  name="atts[hoehe]"
                  value={this.state.hoehe}
                  options={this.props.options.hoehe}
                  onFocus={this.handleFocus.bind(this, 1)}
                  onChange={this.handleSelectChange.bind(this, 'hoehe')} />

               <Select clearable={true}
                  placeholder="Zoll"
                  name="zoll"
                  value={this.state.zoll}
                  options={this.props.options.zoll}
                  onFocus={this.handleFocus.bind(this, 2)}
                  onChange={this.handleSelectChange.bind(this, 'zoll')} />
            </form>
         </div>
      );
   }
}

export default withRouter(FilterBySizeSide);

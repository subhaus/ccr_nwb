import React from 'react';
import ReactDOM from 'react-dom';

import MainMenu from './components/main_menu/MainMenu';
import Sort from './components/Sort';
import TopFilter from './components/TopFilter.js';
import SideFilter from './components/SideFilter.js';
// import FilterByVehicleSide from './forms/FilterByVehicleSide.js';

import Results from './components/Results.js';
import ResultsTeilen from './components/ResultsTeilen.js';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { basePath, getPathname } from './lib/constants';

let path = getPathname();

window.onload = function() {
	ReactDOM.render(<MainMenu sirina={window.screen.width} />, document.getElementById('main_menu_cont'));
}

window.addEventListener('resize', e => {
	ReactDOM.render(<MainMenu sirina={e.currentTarget.screen.width} />, document.getElementById('main_menu_cont'));
});

ReactDOM.render(<TopFilter />, document.getElementById('filter_cont'));

// if(document.getElementById('side_filter') && path !== 'filter' && path !== 'filter_teilen') {
// 	ReactDOM.render(<SideFilter />, document.getElementById('side_filter'));
// }

// ReactDOM.render(<FilterByVehicleSide />, document.querySelector('#side_filter > .clearfix'));

// if(cats.includes(path)) {
// 	ReactDOM.render(<FilterByVehicleSide />, document.querySelector('#side_filter > .clearfix'));
// }

if(document.getElementById('rezultat')) {
	ReactDOM.render(
		<Router basename="/">
			<Switch>
				<Route path={'/filter'} component={Results} />
				<Route path={'/filter_teilen'} component={ResultsTeilen} />
			</Switch>
		</Router>,
		document.getElementById('rezultat')
	);
}


if(document.getElementById('sort')) {
   ReactDOM.render(<Sort />, document.getElementById('sort'));
}
